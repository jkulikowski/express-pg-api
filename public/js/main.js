function valById(domId) {
    return document.getElementById(domId).value; 
};

let doLogout = () => {
    $.get('auth/logout', heartbeat );
    document.getElementById('usersList').innerHTML = '';
    document.getElementById('routes').innerHTML = '';
}

let doLogin = (login, password) => {
    console.log( login, password);
    document.getElementById('login').value = login; 
    document.getElementById('password').value = password;

    $.get(`auth/login/${valById('login')}/${valById('password')}`, heartbeat );
}

let deleteUser = ( uid ) => {
    console.log( uid );

    $.ajax({ url: 'users/:${uid}', type: 'DELETE', success: function(result) {
            console.log( result, ' after delete ');
        }
    });
}

let createUser = () => {
    let data = {
        uid:   null, // or leave it undefined
        login: valById('newLogin'),
        passwd: valById('newPassword'),
        perms: valById('perms') };
    
    $.post( 'users', data, (e, r) => {
        console.log( r );
    });
}

let updateUser = () => {
    let passwd = valById('password'); 
    let data = {    perms:  valById('perms'), 
                    uid:    currentUserUid };

    passwd && (data.passwd = passwd);

    $.ajax({    url: 'users', 
                type: 'PUT', 
                //type: 'POST', 
                data: data, 
                dataType: 'json',
                success: function(result) {
                    console.log( result, ' after delete ');
                }
    });
}

let heartbeat = () => {
    $.get('heartbeat', (response, err) => { 
        let r = JSON.parse( response );
        console.log( 'heartbeat:', r );
        let loggedUser = r.login ? r.login + ':' + r.perms : 'not logged in';

        document.getElementById('userInfo').innerHTML = loggedUser; 
        document.getElementById('database').innerHTML = r.appDatabase;
    });
}

heartbeat();

setTimeout( heartbeat, 2000 );

$.ajaxSetup({
    statusCode: {
        401: function(stuff) {
            console.log( 'got 401', stuff);
            alert('You are not authorized\nLog in with correct permissions');
        },
        500: function(stuff) {
            console.log( 'got 500', stuff.responseText);
        }
    }
});

let currentUserUid = null;
let selectUser = (login, perms, uid) => {
    currentUserUid = uid;

    document.getElementById('selectedUser').innerHTML = login + ' ' + perms;
    document.getElementById('deleteUser').classList.remove('hidden');

    document.getElementById('newLogin').value = login;
    document.getElementById('perms').value = perms;
}

let getUsers = () => {
    $.get('users/all', (r, err, rest) => { 
        console.log( rest , ' rest  ');

        let usersList = JSON.parse( r );
        let html = `<ul class="header"><li>first</li><li>last</li> <li>login</li>
                    <li>email</li><li>perms</li> </ul>
                    `;

        html += usersList.data.map( l => {
            return `
                <ul onclick="selectUser('${l.login}', '${l.perms}', '${l.uid}')">
                    <li> ${l.first}</li>
                    <li> ${l.last}</li>
                    <li> ${l.login}</li>
                    <li> ${l.email}</li>
                    <li> ${l.perms || 'PUB'}</li>
                </ul>
            `;
        }).join('');
        html += `<ul class="hidden" id="deleteUser">
                    <li><button onClick="deleteUser()">
                        Delete <span id="selectedUser"></span>
                    </button></li>
                 </ul>`;

        document.getElementById('usersList').innerHTML = html;
    });
}

let getRoutes = () => {
    $.get('users/getRoutes', (r, err, rest) => { 
        let html = `<ul class="header"><li>endpoint</li><li> method</li>
                    <li>ctrl-call</li><li>params</li><li>perms</li> </ul>`;

        let routes = JSON.parse( r ).data;
        console.log(  routes );
        for ( let i in routes ) {
            html += routes[i].map( l => {
                let cannonical = ['_get', '_post', '_put', '_delete'].indexOf(l[0]) > -1;
                let endPoint   = cannonical ? '' : '/' + l[0];

                return `
                    <ul onclick="selectUser('${l}')">
                        <li> ${i}${endPoint}${l[3] || ''}</li>
                        <li> ${l[1]}</li>
                        <li> ${l[2]}</li>
                        <li> ${l[3] || '&nbsp;'}</li>
                        <li> ${l[4] || 'REG'}</li>
                    </ul>
                `;
            }).join('');
        }
        document.getElementById('routes').innerHTML = html;
    });
}
