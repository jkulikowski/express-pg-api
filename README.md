#Express-Pg-Api
Simple REST API with PostgreSQL supoport using node Express.  It supports sessions with `express-session` module.

## ___Server (quick setup)___

#### 1. Start with
```sh
git clone git@bitbucket.org:jkulikowski/express-pg-api.git
cd express-pg-api
npm install
```

#### 2. Edit file `config.json.sample`, replace `<param>` with values corresponding to your environment
```js
{
    "appDatabase": "<mySql|pgSql>",
    "dbUrl": "postgres://<postgresAccountName>:<postgresAccountPassword>@<server>/<databaseName>",
    "mySql": {
        "host": "localhost",
        "user": "root",
        "password" : "",
        "database" : "<Database Name>"
    },
    "srvPort": "<Server Port Number|8082>",
    ...
}
```
and save the file as **`config.json`**
###You are now ready to start the server
```sh
$ npm start
```
or 
```sh
$ node server.js
```
To run with browser interface
```sh
$ npm run browser
```

***
## ___Defining route controllers___
[https://bitbucket.org/jkulikowski/express-pg-api/src/master/ctrls]

***
## ___Configure gMail (optional)___
[https://bitbucket.org/jkulikowski/express-pg-api/src/master/models]
