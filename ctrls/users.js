const UT       = require('../models/util.js'),
      upd      = require('../models/updates.js'),
      db       = global.appConfig.dbModule,
      dbSchema = require('../models/dbSchema.js'),
      log      = UT.log;

function buildResponse(success, data, msg, statusCode = 200) {
    typeof msg == 'undefined' && (msg = '');
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": msg, "statusCode": statusCode };
}

const me = module.exports = {

        getRoutes: (q, cb) => {
            //console.log( 'get routes app config:', global.appConfig );
            console.log( '>>>>> ctrl getRoutes\n');
            cb(buildResponse(true, global.appConfig.routesTable, 'Ok'));
        },

        heartbeat: (q, cb) => {
            cb(buildResponse(true, global.user, 'Ok'));
        },

        getUser: (q, cb ) => {
            console.log( '>>>>> ctrl getUser uid:>'+ q.uid + '<uid\n');

            let sql  = 'SELECT * FROM users WHERE uid=$1';
            let data = [q.uid];

            db.promiseQuery( sql, data)
            .then( response => {
                cb(buildResponse(true, response, 'All Good'));
            })
            .catch( err => {
                console.log( '............ error in users/getUser');
                cb( buildResponse(false, err, 'DB error', 500) );
            });
        },
        upsertUser: (q, cb) => {
            console.log( 'CRATE USER q', q);

            console.log( 'DEBUG >>>>\n', dbSchema.debug(q, 'users'), '\n\n' );
            
            if (global.appConfig.perms.indexOf( q.perms ) == -1) {
                q.perms = 'REG'; // if perms are missing or invalid set it to REG
            }

            //
            //  Allow only SU and ADM to create and update users
            //  ADM cannot create/update SU
            //  other users can only update their own account
            //
            if  (q.sessionUserPerms != 'SU' 
                    || q.sessionUserPerms == 'ADM' && q.perms == 'SU'
                    || q.sessionUserUid   == q.uid
                ) {
                        cb(buildResponse(true, null,
                                `Not autorhized to create user with permissions ${q.perms}`,
                                401)
                        );
            } else {
                let {sql, data} = dbSchema.genUpsert(q, 'users');
                console.log( "RETURNED form dbSchema.gen\n", sql, data);

                db.promiseQuery(sql, data)
                .then( response => {
                    cb(buildResponse(true, [], 'good', 200));
                })
                .catch( err => {
                    console.log( '............ error in createUser');
                    cb( buildResponse(false, err, 'DB error', 500) );
                });
            }
        },
        findUser: (q, andAuthenticate = false) => new Promise( (y, n) => {
            let sql  = q.from_google ? "SELECT * FROM users WHERE email=$1"
                                     : "SELECT * FROM users WHERE login=$1",

                data = q.from_google ? [q.email] : [q.login]; 

            if (andAuthenticate && !q.from_google) {
                sql += ' AND passwd=$2';
                data.push(q.passwd);
            }

            UT.qCb( sql, data, resp => resp.data.length ? y( resp.data[0] ) : n([]) );
        }),
        allUsers: function(q, cb) {
            var sql = "SELECT uid, first, last, email, perms, auth, login FROM users";
            var data = [];

            db.promiseQuery(sql, data)
            .then( allUsers => {
                cb(buildResponse(true, allUsers, ''));
            });
        },
        
        changePasswd: (q, cb) => {
            let sql = "UPDATE users SET passwd=$1 WHERE uid=$2";
            let data = [q.passwd, q.uid];

            console.log( 'sending sql:', sql);

            db.promiseQuery( sql, data )
            .then( response => {
                cb(buildResponse({"success": true, "data": response}))
            })
            .catch( e => {
                console.log( e );
                cb(buildResponse(false, null, "Error while changing password", 500))
            });
        },

        delUser: (q, cb) => UT.qCb(
            "DELETE FROM users where uid=$1", [q.uid], 
            () => me.allUsers(q, cb)
        ),

        getMyCreds: function(myUid, cb) {
            var sql = db.qFormat("SELECT first, last, phone FROM users WHERE uid='{0}'", [myUid]);

            db.q(sql, res => cb(resp(res[0], true)),
                       ex => cb(resp(ex, false)));
        },

        sendMail: function(to, aHref, subject, text, attach) {
            msg.sendMail(to, aHref, subject, text, attach);
        }
};
