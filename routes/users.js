var mod = require('../ctrls/users.js')

var output =  (q, cb, modMethod) => {
    console.log( 'MOD METHOD:', modMethod );
    return mod[modMethod](q, response => {
        cb( response )
    })
};

module.exports = {
    all :           output, //ROUTER ['GET', 'allUsers', '', 'SU']
    getRoutes :     output, //ROUTER ['get', 'getRoutes', '', 'SU']

    _get:    output, //ROUTER ['get',    'getUser', '/:uid']
    _delete: output, //ROUTER ['delete', 'delUser', '/:uid', 'ADM']
    _put:    output, //ROUTER ['put',    'upsertUser', '', 'ADM']
    _post:   output  //ROUTER ['post',   'upsertUser', '', 'ADM']
}
