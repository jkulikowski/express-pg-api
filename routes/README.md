## _Configure gMail (optional)_
Let's take another look at `config.json`
```javascript
{
    "url": "postgres://<postgresAccountName>:<postgresAccountPassword>@<server>/<databaseName>",
    "srvPort": "<Server Port Number|8080>",
    "gmailCreds" : {
        "installed":
            {
                "client_id":"<CLIENT ID|305424103139-oa2k4ac9231kajt7ks9as1kolarv2kpa.apps.googleusercontent.com",
                "client_secret": "<CLIENT SECRET|tlieka4poRVz9tu91mnoKSi3>",
                "project_id": "<PROJECT ID>",
                "auth_uri":   "https://accounts.google.com/o/oauth2/auth",
                "token_uri":  "https://accounts.google.com/o/oauth2/token",
                "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob","http://localhost"]
        }
    }
}
```
Here replace

 Param    | Example
 -------- | -------
 **CLIENT ID** | 305424103139-oa2k4ac9231kajt7ks9as1kolarv2kpa.apps.googleusercontent.com 
 **CLIENT SECRET** | tlieka4poRVz9tu91mnoKSi3
 **PROJECT ID** | your project id [string]


### Get those parameters from, or set them up on:
[Google Cloud Platform][https://console.cloud.google.com]

From the right menu select `'APIs & Servces' -> Credentials`
