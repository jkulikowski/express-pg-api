let fs  = require('fs'),
    pg  = require('mysql'),
    upd = require('./updates.js'),
    UT  = require('./util.js');

let mysql = require('mysql');

let myDb = mysql.createConnection(
    global.appConfig.mySql
);

myDb.connect();

let getConn = cb => {
    cb(myDb);
}

let retries = 9;

let resp = (data, success, msg) => {
    typeof msg == 'undefined' && (msg = '');
    success || UT.log.err('DB ERROR', data);
    return {"data": data, "success": success, "msg": msg};
}

let me;
module.exports = me = {
    //
    // Functions query and promiseQuery
    // Arguments:
    //      sql   - sql query with field valud replacements ($1, $2, ...)
    //      data  - array of corresponding values
    //      cb - is the standard callback function returning result rows in (query only)
    //
    //      midCb - optional middleware callback
    //              it accepts query rows and returns them in the same format after processing
    //
    query: (sql, data, cb, midCb = null) => {
        myDb.query(sql, data, (e, r) => {
            console.log( r );
            let result = typeof midCb == 'function' ? midCb( r ) : r;
            e || typeof cb == 'function' && cb(result);
        })
    },
    promiseQuery: function(sql, data, midCb) { return  new Promise( (y, n) => {
        sql = sql.replace(/\$\d+/g, '?'); //convert postges call

        myDb.query(sql, data, (e,r) => {
            if (e) {
                n( e );
            } else {
                let result = typeof midCb == 'function' ? midCb(r) : r;
                y( result );
            }
        })
    })},
    //
    // real - used for debugging purposes
    //
    real: (sql, data, typeA)  => {
        for (var i=1; i<20; i++){
            var quote = typeof typeA == 'object' && typeof typeA[i-1] != 'undefined' && typeA[i-1]
                        ? "" : "'";

            if (sql.indexOf('$' + i) == -1)
                break;
            else
                sql = sql.replace(new RegExp('\\$' + i.toString(), 'g'), quote + data[i-1] + quote);
        }
        return sql;
    },
    getData: (sql, data, cb) => {
        UT.log.lg( sql, data,  'db.get' );
        UT.log.dbg( me.real(sql, data),  'db.get' );

        myDb.query( me.real(sql, data), [], r => UT.log.err( r ) );

        myDb.query(sql, data, result => {
            UT.log.warn('result', result);
            cb(resp(result, true));
        }, err => {
            UT.log.err(sql, data, err);
            cb(resp('ERROR while getting from ' + tab, false));
        });
    },
    save: (tab, q, cb) => {
        var g = upd.gen(q, tab);
        UT.log.lg( g, ' upd data sql' );

        myDb.query(g.sql, g.data, result => {
            console.log ( q.sql, result , ' result save db' );
            cb(resp(result, true));
        }, err => {
            UT.log.err(g, err);
            cb(resp('ERROR while saving to ' + tab, false));
        });
    }
}

//
// INITIALIZATION
//
// Start database connection and validate required tables
//
let usersTableName = 'users';
let defaultCheck =  {

    findTablesSql: `
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema = '${global.appConfig.mySql.database}'
            AND (table_name = 'sessions' OR table_name ='${usersTableName}')`,

    createUsersTableSql: `
        create table ${usersTableName} (
            uid int primary key not null auto_increment,
            login   varchar(255),
            passwd  varchar(255),
            auth    varchar(4),
            perms   varchar(4),
            first   varchar(32),
            last    varchar(32),
            email   varchar(255)
        )`,

    createSessionTableSql: `
        CREATE TABLE IF NOT EXISTS \`sessions\` (
            session_id varchar(128) COLLATE utf8mb4_bin NOT NULL,
            expires int(11) unsigned NOT NULL,
            data text COLLATE utf8mb4_bin,
            PRIMARY KEY (session_id)
        ) ENGINE=InnoDB`
}

//
// Initialize connection and run a quick test to see if users and session tables exist
//
getConn( () => {
    myDb.query(defaultCheck.findTablesSql, [], (e,r) => {
        console.log( r, ' r ' );
        console.log( e, ' e ' );
        if (e) {
            console.log( 'error:', e );
        } else {
            let found = r.filter( t => [usersTableName, 'sessions'].indexOf(t.table_name) > -1)
                         .map(    t => t.table_name );

            console.log( found, ' found default tables ');

            if (found.indexOf( usersTableName  ) > -1) {
                UT.glog( 'OK', `TABLE ${usersTableName} -` );
            } else {
                UT.brlog('Creating table ' + usersTableName);
                myDb.query(defaultCheck.createUsersTableSql, [], (e,r) => {
                    if (e) {
                        console.log( e );
                        UT.rlog(`Create Table ${usersTableName} users Error`);
                    } else {
                        UT.glog('Table users created - OK');
                        myDb.query(`INSERT INTO ${usersTableName} 
                                    (login, passwd, first, last, perms) VALUES (?, ?, ?, ?, ?)`,
                                    ['admin', 'change-me', 'admin', 'Admin', 'SU']);
                    }
                });
            }

            if (found.indexOf( 'sessions' ) > -1) {
                console.log( e );
                UT.glog( 'OK', 'TABLE sessions -' );
            } else {
                UT.brlog('Creating table sessions');
                myDb.query(defaultCheck.createSessionTableSql, [], (e,r) => {
                    if (e) {
                        console.log( e );
                        UT.rlog('Create Table sessions Error');
                    } else {
                        UT.glog('Table sessions created - OK');
                    }
                });
            }
        }
    });
});
