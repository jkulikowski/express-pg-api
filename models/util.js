let request = require('request');

// POLYFILL until new version
if (false)
if (!Array.from) {
    Array.from = function (object) {
        'use strict';
        return [].slice.call(object);
    };
}

var path = __dirname.split('/');
var models = path.pop();
var dirName = path.pop();

var c = {
    black           :'\x1b[30m',
    blue            :'\x1b[34m',
    green           :'\x1b[32m',
    cyan            :'\x1b[36m',
    red             :'\x1b[31m',
    purple          :'\x1b[35m',
    brown           :'\x1b[33m',
    gray            :'\x1b[37m',
    end             :'\x1b[0m'
};

function logHeart(arg) {
    try {
        require('fs').appendFile('./log/heartbeat.log', c.gray + arg + c.end + '\n');
    } catch (e) {
        console.log(c.red +  e.stack + c.end);
    }
}
function doLog(arg, color) {
    try {
        //console.log(color, JSON.stringify(Array.from(arg).join(), null, 4), '\x1b[0m');
        console.log(color, JSON.stringify(arg, null, 4), '\x1b[0m');
    } catch (e) {
        console.log(c.red +  e.stack + c.end);
    }
}

function resp(data, success, msg) {
    success || console.log( c.red + 'DB ERROR', data, c.end);
    return {"data": data, "success": success, "msg": msg};
}


let db = null;

var me = {
    setDb: inDb => db = inDb,

    deep: function(arg)        { return JSON.parse(JSON.stringify(arg)); },
    isAdm :    function(perms) { return perms.substring(0,3) === 'ADM'; },
    isReg :    function(perms) { return perms.substring(0,3) === 'REG'; },
    isClient : function(perms) { return perms.substring(0,3) === 'CLI'; },

    parseRequest: function(r, data) {
        var url = require('url');
        var q   = data ? data : url.parse(r.url, true).query;
        var p   = url.parse(r.url, true).pathname.split('/');

        p[0]   == '' && p.shift(); 
        return {p: p, q: q, h: r.headers}
    },

    getColors: function() { return c; },
    c: c,
    colors:  c,
    ucfirst: function(inStr) {
        return inStr.charAt(0).toUpperCase() + inStr.slice(1);
    },
    //getDirName: function() { return global.appConfig.inst; },
    guid : function() {
        return 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    passwd: function() {
        return 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    resp:  resp,
    isUuid: function(uid) {
        return  uid.toString().match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i);
    },
    // Log
    log: {
        lg:     function () { doLog(arguments, '\x1b[32m'); },
        dbg:    function () { doLog(arguments, '\x1b[36m'); },
        warn:   function () { doLog(arguments, '\x1b[33m'); },
        info:   function () { doLog(arguments, '\x1b[34m'); },
        err:    function () { doLog(arguments, '\x1b[31m'); },
        glog:   function (arg) { console.log(c.green,   arg, c.end); },
        rlog:   function (arg) { console.log(c.red,     arg, c.end); },
        blog:   function (arg) { console.log(c.blue,    arg, c.end); },
        plog:   function (arg) { console.log(c.purple,  arg, c.end); },
        heart:  function (s) { logHeart(s)           }
    }, 

    glog: (arg, lab='') => console.log((lab ? lab :''), c.green,  arg, c.end),
    rlog: (arg, lab='') => console.log((lab ? lab :''), c.red,    arg, c.end),
    plog: (arg, lab='') => console.log((lab ? lab :''), c.purple, arg, c.end),
    blog: (arg, lab='') => console.log((lab ? lab :''), c.blue,   arg, c.end),
    brlog: (arg, lab='') => console.log((lab ? lab :''), c.brown,   arg, c.end),

    valid : {
        email: function(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        uid: function(uid) {
            return  uid.toString().match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i);
        }
    },
    db: {
        queryAndCb: function(sql, data, cb, midCb) {
            db.query(sql, data, userRows => { 

                if ( typeof cb == 'function' )
                    typeof midCb == 'function'  ? cb(resp(midCb(userRows), true, 'OK'))
                                                : cb(resp(userRows, true, 'OK')); 
            }, e => {
                console.log('query returned error',  e, sql);
                if ( typeof cb == 'function' )
                    cb(resp(e, false, 'DB_ERR')); 
           });
        },
        transaction: (sql, cb) => {
            db.query('BEGIN;' + sql, [], transResult => {
                db.q('COMMIT',  goodCommit => cb([true, transResult]), 
                                badCommit  => cb([false, badCommit])
                );
            }, err => {
                db.q('ROLLBACK', () => cb([false, err.toString()]),
                                 () => cb([false, err.toString()])
                );
            });
        },
        upd: function(q, tab, cb, midCb) {
            if (q.uid == null) delete q.uid;

            var p = require('./updates.js').gen(q, tab);

            global.appConfig.getReal && me.blog( p.sql, 'UPD: ' );

            me.db.queryAndCb(p.sql, p.data, cb, midCb);
        },
        oplog: function(label, data, params) {
            typeof params == 'undefined' && (params = '');
            var sql ="INSERT INTO oplog (label, params, data, created) VALUES ($1, $2, $3, now()) RETURNING uid, label";
            db.query(sql, [label, params, data], 
                oplogRet => console.log( ' oplog ret ', oplogRet ),
                err      => console.log( err ) );
        },
        testQ: ( sql, args, skipQuotes = []) => {
            for ( let i=0; i<args.length; i++ ) {
                let re = new RegExp( '\\$' + (i+1), 'g'),
                    qt = skipQuotes[i] ? '' : "'";

                sql = sql.replace(re, `${qt}${args[i]}${qt}`);
            }

            me.plog( sql );
        }
    },
    callAction: function(mod, action, res, q, auth) {
        if (typeof auth == 'undefined' || auth.indexOf(res.sessionUser.creds.split(':')[0]) > -1) {
            q.clinic_uid = res.sessionUser.clinic_uid; 
            q.user_uid   = res.sessionUser.uid;
            mod[action](q, function(r) { res.end(JSON.stringify(r)); });
        } else {
            res.end('{"success": true, "msg": "Insuficient Permissions", "c_uid":"' 
                + res.sessionUser.clinic_uid + '", "valid_resp": false}');
        }
    },
    getPromise: ( uri, auth ) => new Promise( (y, n) =>
        request.get({ uri:  uri }, (err, rawResp, body) => 
            err ? n( body ) : y( body ) ) 
        )
}
me.qCb = me.db.queryAndCb;
me.testQ = me.db.testQ;
module.exports = me;
