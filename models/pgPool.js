    // by default the pool uses the same
    // configuration as whatever `pg` version you have installed

    /*
    // you can pass properties to the pool
    // these properties are passed unchanged to both the node-postgres Client constructor
    // and the node-pool (https://github.com/coopernurse/node-pool) constructor
    // allowing you to fully configure the behavior of both
    let pool2 = new Pool({
      database: 'postgres',
      user: 'brianc',
      password: 'secret!',
      port: 5432,
      ssl: true,
      max: 20, // set pool max size to 20
      min: 4, // set min pool size to 4
      idleTimeoutMillis: 1000, // close idle clients after 1 second
      connectionTimeoutMillis: 1000, // return an error after 1 second if connection could not be established
    };)

    //you can supply a custom client constructor
    //if you want to use the native postgres client
    let NativeClient = require('pg').native.Client;
    let nativePool = new Pool({ Client: NativeClient });

    //you can even pool pg-native clients directly
    let PgNativeClient = require('pg-native');
    let pgNativePool = new Pool({ Client: PgNativeClient });
    */

let Pool = require('pg-pool');
let pool = null;

let me = module.exports = {
    init: () => {
        let conn  = require('url').parse(global.appConfig.dbUrl);
        let auth  = conn.auth.split( ':' );

        const config = {
            user: auth[0],
            password: auth[1],
            host: conn.hostname,
            port: conn.port,
            database: conn.pathname.split('/')[1],
            ssl: true
       };

       pool = new Pool( config );
    },
    query: (sql, data = []) => new Promise( (y, n) => {
        pool.query(sql, data)
        .then( resp => {
            y( resp.rows );
        })
        .catch( e => {
            console.log( 'POOL error', e) 
            n(e);
        });
    })
}

module.exports.init();
