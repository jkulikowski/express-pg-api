var UT=require('./util.js');

const TXT =0;
const UID =1;
const INT =2;
const UPD =3;
const BOOL=4;
const JSN =5;
const DAT =6;
const TIM =7;
const CHR =8;
const NUM =9;

var fields = {
    'oplog':    {
        label:          CHR,
        data:           JSN
    },
    'users' : {
        first       : CHR,
        last        : CHR,
        phone       : CHR,
        email       : CHR,
        passwd      : CHR,
        login       : CHR,
        perms       : CHR
    },
    'proj' : {
        user_uid:   UID,
        config:     JSN,
        caps:       JSN,
        handle:     CHR,
        ver:        CHR,
        is_public:  BOOL 
     },
    'gens' : {
        user_uid:   UID,
        sub_uid:    UID,
        ip:         CHR,
        title:      CHR,
        //data:       JSN,
        dl_count:   NUM,
        ver:        CHR
     },
     "ppal" : {
        user_uid:   UID,
        email:      CHR,
        payment_id: CHR,
        amount:     NUM,
        send:       JSN,
        completed:  JSN,
        hook:       JSN,
        is_sand:    BOOL,
        payer_id:   CHR
     },
     "quotas": {
        user_uid:    UID,
        plan:        CHR,
        period:      CHR,
        plan_expiry: CHR,
        quota:       NUM,
        lock_quota:  BOOL,
        amount:      NUM
    },
     "feedback": {
        user_uid:   UID,
        ref_uid:    UID,
        content:    CHR,
        email:      CHR
    }
};

module.exports = {
    cre: function(key) {
        var sql = 'CREATE TABLE ' + key + ' ( uid uuid not null default uuid_in((md5(((random())::text || (now())::text)))::cstring),';
        var f = fields[key];
        var out = '';
        for (var i in f) {
            switch (f[i]) {
                case CHR:  out += ',\n' + i + ' varchar(128)'; break;
                case TXT:  out += ',\n' + i + ' text'; break;
                case UID:  out += ',\n' + i + ' uuid'; break;
                case INT:  out += ',\n' + i + ' integer'; break;
                case BOOL: out += ',\n' + i + ' boolean'; break;
                case DAT:  out += ',\n' + i + ' date'; break;
                case JSN:  out += ',\n' + i + ' text'; break;
                case TIM:  out += ',\n' + i + ' timestamp'; break;
            }
        }
        sql += out.substring(3) + ');';
    },

    real: function(q, key) {
        var f = fields[key];

        var isUpdate = typeof q.uid != 'undefined' && UT.isUuid(q.uid);
        isUpdate || (f.created = UPD);

        var ret = {vals: '', flds: '', data: [], warn:[]};
        for (var i in f)
            if (typeof q[i] != 'undefined') {
                    var dataVal = [INT, BOOL].indexOf(f[i]) > -1 ? q[i] : "'" + q[i] + "'";
                    if (isUpdate) {
                        ret.flds += ', ' + i + '=' + dataVal + (f[i] == UID ? '::uuid' : '');
                    } else {
                        ret.flds += ', ' + i;
                        ret.vals += ', ' + dataVal + (f[i] == UID ? '::uuid' : '');
                    }
            }

        if (isUpdate) {
            ret.flds += ', updated=now()';
        } else {
            ret.flds += ', created ';
            ret.vals += ', now()';
        }
        ret.flds = ret.flds.substring(2);
        ret.vals = ret.vals.substring(2);

        var sql = '';
        if (isUpdate) {
            ret.data.push(q.uid);
            sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid='" + q.uid + "'::uuid RETURNING *";
        } else {
            sql =  "INSERT INTO " + key +  " (" + ret.flds + ") VALUES (" + ret.vals + ") RETURNING *";
        }

        ret.sql = sql;

        global.appConfig.getREAL && UT.plog( ret.sql,  'REAL: ' );
    },
    gen: function(q, key) {
        module.exports.real(q, key);

        var f = fields[key];

        var isUpdate = typeof q.uid != 'undefined' && UT.isUuid(q.uid);
        isUpdate || (f.created = UPD);

        var ret = {vals: '', flds: '', data: [], warn:[]};
        var idx = 1;
        for (var i in f)
            if (typeof q[i] != 'undefined') {
                    if (isUpdate) {
                        ret.flds += ', ' + i + '=$' + idx + (f[i] == UID ? '::uuid' : '');
                    } else {
                        ret.flds += ', ' + i;
                        ret.vals += ', $' + idx + (f[i] == UID ? '::uuid' : '');
                    }

                    if (f[i] == DAT) {
                        var matched = q[i].match(/^[1-9]{4}-[0-9]{2}-[0-9]{2}$/g);
                    }
                        
                    ret.data.push(f[i] === INT 
                        ? parseInt(q[i])
                        : (f[i] === JSN ? JSON.stringify(q[i]) : q[i]));
                    idx++;
            }

        if (isUpdate) {
            ret.flds += ', updated=now()';
        } else {
            if (ret.flds.indexOf('created') == -1) {
                ret.flds += ', created ';
                ret.vals += ', now()';
            }
        }

        ret.flds = ret.flds.substring(2);
        ret.vals = ret.vals.substring(2);
        ret.last = '$' + idx;

        var sql = '';
        if (isUpdate) {
            ret.data.push(q.uid);
            sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid=$" + idx + "::uuid RETURNING *";
        } else {
            sql =  "INSERT INTO " + key +  " (" + ret.flds + ") VALUES (" + ret.vals + ") RETURNING *";
        }

        ret.sql = sql;

        return ret;
    }
}
